package com.wipro.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "student")	
public class Student implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5744479831176348325L;

	private String studentId;

	private String studentName;

	private String studentClass;

	private String studentTotalMarks;

	public Student() {
	};

	public Student(String studentId, String studentName, String studentClass, String studentTotalMarks) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentClass = studentClass;
		this.studentTotalMarks = studentTotalMarks;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(String studentClass) {
		this.studentClass = studentClass;
	}

	public String getStudentTotalMarks() {
		return studentTotalMarks;
	}

	public void setStudentTotalMarks(String studentTotalMarks) {
		this.studentTotalMarks = studentTotalMarks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((studentClass == null) ? 0 : studentClass.hashCode());
		result = prime * result + ((studentId == null) ? 0 : studentId.hashCode());
		result = prime * result + ((studentName == null) ? 0 : studentName.hashCode());
		result = prime * result + ((studentTotalMarks == null) ? 0 : studentTotalMarks.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (studentClass == null) {
			if (other.studentClass != null)
				return false;
		} else if (!studentClass.equals(other.studentClass))
			return false;
		if (studentId == null) {
			if (other.studentId != null)
				return false;
		} else if (!studentId.equals(other.studentId))
			return false;
		if (studentName == null) {
			if (other.studentName != null)
				return false;
		} else if (!studentName.equals(other.studentName))
			return false;
		if (studentTotalMarks == null) {
			if (other.studentTotalMarks != null)
				return false;
		} else if (!studentTotalMarks.equals(other.studentTotalMarks))
			return false;
		return true;
	}

}
