CREATE TABLE employees(
	empId VARCHAR(100), 
    empFirstName VARCHAR(100), 
	empLastName VARCHAR(100),
	empLocation VARCHAR(100),
	empBand VARCHAR(100)
);