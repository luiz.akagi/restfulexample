package com.wipro.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.wipro.domain.Employee;

public class EmployeeDao {

	static final String JDBC_DRIVER = "org.h2.Driver";
	static final String DB_URL = "jdbc:h2:mem:testdb";
	// Database credentials
	static final String USER = "sa";
	static final String PASS = "";

	public List<Employee> findAll() {

		List<Employee> employees = new ArrayList<>();

		String query = "SELECT * FROM employees;";

		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			Employee employee;
			while (rs.next()) {
				employee = new Employee();

				employee.setEmpId(rs.getString("empId"));
				employee.setEmpFirstName(rs.getString("empFirstName"));
				employee.setEmpLastName(rs.getString("empLastName"));
				employee.setEmpLocation(rs.getString("empLocation"));
				employee.setEmpBand(rs.getString("empBand"));
				employees.add(employee);
			}

			conn.close();
		} catch (Exception dae) {
			dae.printStackTrace();
		}

		return employees;
	}

	public boolean save(Employee employee) {

		boolean ret = false;
		String query = "INSERT INTO employees (empId,empFirstName,empLastName,empLocation,empBand) VALUES (?,?,?,?,?);";
		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			PreparedStatement st = conn.prepareStatement(query);
			st.setString(1, employee.getEmpId());
			st.setString(2, employee.getEmpFirstName());
			st.setString(3, employee.getEmpLastName());
			st.setString(4, employee.getEmpLocation());
			st.setString(5, employee.getEmpBand());

			ret = st.executeUpdate() > 0;
			
		} catch (Exception dae) {
			dae.printStackTrace();
		}

		return ret;
	}

	public Employee findByEmpLocation(String empLocation) {

		String sql = "SELECT * FROM employees WHERE empLocation=?";

		Employee employee = new Employee();

		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(1, empLocation);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				employee.setEmpId(rs.getString("empId"));
				employee.setEmpFirstName(rs.getString("empFirstName"));
				employee.setEmpLastName(rs.getString("empLastName"));
				employee.setEmpLocation(rs.getString("empLocation"));
				employee.setEmpBand(rs.getString("empBand"));
			}
			
		} catch (Exception dae) {
			dae.printStackTrace();
		}

		return employee;
	}
	
	public Employee findByEmpBand(String empBand) {

		String sql = "SELECT * FROM employees WHERE empBand=?";

		Employee employee = new Employee();

		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(1, empBand);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				employee.setEmpId(rs.getString("empId"));
				employee.setEmpFirstName(rs.getString("empFirstName"));
				employee.setEmpLastName(rs.getString("empLastName"));
				employee.setEmpLocation(rs.getString("empLocation"));
				employee.setEmpBand(rs.getString("empBand"));
			}
			
		} catch (Exception dae) {
			dae.printStackTrace();
		}

		return employee;
	}

	public boolean update(Employee employee) {


		String sql = "UPDATE employees SET empFirstName=?, empLastName=?, empLocation=?,empBand=?  WHERE empId=?";
		boolean ret = false;

		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(1, employee.getEmpFirstName());
			st.setString(2, employee.getEmpLastName());
			st.setString(3, employee.getEmpLocation());
			st.setString(4, employee.getEmpLocation());
			st.setString(5, employee.getEmpId());

			ret = st.execute();
			
		} catch (Exception dae) {
			dae.printStackTrace();
		}

		return ret;
	}

	public boolean delete(String empId) {

		boolean ret = false;

		String sql = "DELETE FROM employees WHERE empId=?";
		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(1, empId);
			ret = st.execute();
			
		} catch (Exception dae) {
			dae.printStackTrace();
		}

		return ret;
	}
}
