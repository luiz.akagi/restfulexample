package com.wipro.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.stereotype.Service;

import com.wipro.domain.Student;

@Service
public class StudentService {

	ArrayList<Student> listOfStudents;

	public StudentService() {
		listOfStudents = new ArrayList<Student>();
	}

	public ArrayList<Student> getAllStudents() {
		return listOfStudents;
	}

	public Student getStudent(String id) {
		for (Student studentAux : listOfStudents) {
			if (studentAux.getStudentId().equals(id)) {
				return (studentAux);
			}
		}
		return null;
	}

	public List<Student> getStudentByTotalMark(String id) {

		List<Student> result = new ArrayList<Student>();
		Predicate<Student> cutMark = (n) -> Double.parseDouble(n.getStudentTotalMarks()) > Double.parseDouble(id);
		listOfStudents.stream().filter(cutMark).forEach((n) -> result.add(n));

		return result;
	}

	public ArrayList<Student> getStudentByClass(String id) {

		ArrayList<Student> result = new ArrayList<Student>();
		Predicate<Student> studentClass = (n) -> n.getStudentClass().equalsIgnoreCase(id);
		listOfStudents.stream().filter(studentClass).forEach((n) -> result.add(n));

		return result;
	}

	public void addStudent(Student student) {
		listOfStudents.add(student);
	}

	public boolean removeStudent(String id) {
		for (Student studentAux : listOfStudents) {
			if (studentAux.getStudentId().equals(id)) {
				listOfStudents.remove(listOfStudents.indexOf(studentAux));
				return true;
			}
		}
		return false;
	}

	public Student updateStudent(Student student) {
		for (Student studentAux : listOfStudents) {
			if (studentAux.getStudentId().equals(student.getStudentId())) {
				listOfStudents.set(listOfStudents.indexOf(studentAux), student);
				return listOfStudents.get(listOfStudents.indexOf(student));
			}
		}
		listOfStudents.add(student);
		return student;
	}

}
