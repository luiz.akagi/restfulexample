package com.wipro.service;

import java.util.List;

import com.wipro.SpringApplicationContext;
import com.wipro.dao.EmployeeDao;
import com.wipro.domain.Employee;

public class EmployeeService {

	private EmployeeDao employeeDao;

	public EmployeeService() {
		employeeDao = (EmployeeDao) SpringApplicationContext.getBean("employeeDao");
	}

	public List<Employee> findAll() {
		return employeeDao.findAll();
	}

	public Employee findByEmpLocation(String empLocation) {

		return employeeDao.findByEmpLocation(empLocation);
	}

	public Employee findByEmpBand(String empBand) {

		return employeeDao.findByEmpBand(empBand);
	}

	public boolean save(Employee employee) {

		return employeeDao.save(employee);
	}

	public boolean update(Employee employee) {

		return employeeDao.update(employee);
	}

	public boolean delete(String empId) {

		return employeeDao.delete(empId);
	}
}
