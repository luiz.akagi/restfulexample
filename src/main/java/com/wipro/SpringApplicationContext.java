package com.wipro;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringApplicationContext implements ApplicationContextAware {

	private static ApplicationContext appContext;
	static final String JDBC_DRIVER = "org.h2.Driver";
	static final String DB_URL = "jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;" + "INIT=RUNSCRIPT FROM 'classpath:/sql/schema.sql'"
				+ "\\;RUNSCRIPT FROM 'classpath:/sql/data.sql'";
	// Database credentials
	static final String USER = "sa";
	static final String PASS = "";

	// Private constructor prevents instantiation from other classes
	private SpringApplicationContext() {
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		appContext = applicationContext;


		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL,USER,PASS);
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static Object getBean(String beanName) {
		return appContext.getBean(beanName);
	}

}