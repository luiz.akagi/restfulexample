package com.wipro.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "employee")
public class Employee implements Serializable {

	private static final long serialVersionUID = 5748788901681573813L;
	private String empId;
	private String empFirstName;
	private String empLastName;
	private String empLocation;
	private String empBand;

	public Employee() {

	}

	public Employee(String empId, String empFirstName, String empLastName, String empLocation, String empBand) {
		super();
		this.empId = empId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empLocation = empLocation;
		this.empBand = empBand;
	}



	public String getEmpId() {
		return empId;
	}
	
	@XmlElement
	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	@XmlElement
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	@XmlElement
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getEmpLocation() {
		return empLocation;
	}

	@XmlElement
	public void setEmpLocation(String empLocation) {
		this.empLocation = empLocation;
	}

	public String getEmpBand() {
		return empBand;
	}

	@XmlElement
	public void setEmpBand(String empBand) {
		this.empBand = empBand;
	}

}
