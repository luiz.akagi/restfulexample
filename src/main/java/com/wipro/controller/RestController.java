package com.wipro.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.ws.http.HTTPException;

import org.apache.commons.httpclient.HttpConstants;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;

import com.wipro.SpringApplicationContext;
import com.wipro.domain.Employee;
import com.wipro.domain.Movie;
import com.wipro.domain.Student;
import com.wipro.service.EmployeeService;
import com.wipro.service.MovieService;
import com.wipro.service.StudentService;

@Path("/")
public class RestController {

	private static final String MOVIE_SERVICE = "movieService";

	private static final String STUDENT_SERVICE = "studentService";

	MovieService movieService;
	
	StudentService studentService;
	
	EmployeeService employeeService = (EmployeeService) SpringApplicationContext.getBean("employeeService");
	
	@GET
	@Path("/MovieService/movies_text_xml")
	@Produces(MediaType.TEXT_XML)
	public List<Movie> getMoviesTextXML() {
		movieService = (MovieService) SpringApplicationContext.getBean(MOVIE_SERVICE);
		return movieService.getMovies();
	}
	
	@GET
	@Path("/MovieService/movies_application_xml")
	@Produces(MediaType.APPLICATION_XML)
	public List<Movie> getMoviesApplicationXML() {
		movieService = (MovieService) SpringApplicationContext.getBean(MOVIE_SERVICE);
		return movieService.getMovies();
	}
	
	@GET
	@Path("/MovieService/movies_application_json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMoviesApplicationJSON() {
		movieService = (MovieService) SpringApplicationContext.getBean(MOVIE_SERVICE);
		List<Movie> result = movieService.getMovies();
		GenericEntity<List<Movie>> entity = new GenericEntity<List<Movie>>(result){};
		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/StudentService/students")
	@Consumes({MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_XML})
	public Response getApplicationXmlValue() {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		List<Student> response = studentService.getAllStudents();
		GenericEntity<List<Student>> entity = new GenericEntity<List<Student>>(response){};
		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/StudentService/students")
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getApplicationJsonValue() {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		List<Student> response = studentService.getAllStudents();
		GenericEntity<List<Student>> entity = new GenericEntity<List<Student>>(response){};
		return Response.ok(entity).build();
	}

	@GET
	@Path("/StudentService/student/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentById(@PathParam(value = "id") String id) {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		Student response = studentService.getStudent(id);
		GenericEntity<Student> entity = new GenericEntity<Student>(response){};
		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/StudentService/studentsByMark/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentByTotalMark(@PathParam(value = "id") String id) {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		List<Student> response = studentService.getStudentByTotalMark(id);
		GenericEntity<List<Student>> entity = new GenericEntity<List<Student>>(response){};
		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/StudentService/students/Class/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentByClass(@PathParam(value = "id") String id,@QueryParam("studentClass") String query, @MatrixParam("studentClass") String studentClass) {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		List<Student> response = new ArrayList<Student>();
		
		response.addAll(studentService.getStudentByClass(id));
		response.addAll(studentService.getStudentByClass(query));
		response.addAll(studentService.getStudentByClass(studentClass));
		
		GenericEntity<List<Student>> entity = new GenericEntity<List<Student>>(response){};
		return Response.ok(entity).build();
	}
	
	@POST
	@Path("/StudentService/student")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApplicationXmlValue(@RequestBody Student student) {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		studentService.addStudent(student);
		return Response.ok("{\"Response\":\"Successfully Inserted\"}").build();
	}
	
	@DELETE
	@Path("/StudentService/student/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteStudentById(@PathParam(value = "id") String id) {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		if (studentService.removeStudent(id)){
			return Response.ok("{\"Response\":\"Successfully Deleted\"}").build();
		} else {
			return Response.ok("{\"Response\":\"Not able to delete by id\"}").build();
		}
	}
	
	@PUT
	@Path("/StudentService/student")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStudent(@RequestBody Student student) {
		studentService = (StudentService) SpringApplicationContext.getBean(STUDENT_SERVICE);
		Student response = studentService.updateStudent(student);
		GenericEntity<Student> entity = new GenericEntity<Student>(response){};
		return Response.ok(entity).build();
	}
	
	
	@GET
	@Path("/CardNumberService/{pan}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getValidePan(@PathParam(value = "pan") String pan) {
		
		if (pan.endsWith("6")) {
			return Response.ok("{\"Response\":\"Valid Card Number\"}").build();
		} else {
			return Response.ok("{\"Response\":\"invalid Card Number\"}").build();
		}
	}
	
	
	@GET
	@Path("/EmployeeService/employees")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEmployeesJson() {
		
		List<Employee> response = employeeService.findAll();
		GenericEntity<List<Employee>> entity = new GenericEntity<List<Employee>>(response){};
		return Response.ok(entity).build();
		
	}
	
	@GET
	@Path("/EmployeeService/employees")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getEmployeesXml() {
		
		List<Employee> response = employeeService.findAll();
		GenericEntity<List<Employee>> entity = new GenericEntity<List<Employee>>(response){};
		return Response.ok(entity).build();
		
	}
	
	@GET
	@Path("/EmployeeService/employeesByLocation/{empLocation}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEmployeesByEmpLocationJson(@PathParam(value = "empLocation") String empLocation) {
		
		Employee response = employeeService.findByEmpLocation(empLocation);
		GenericEntity<Employee> entity = new GenericEntity<Employee>(response){};
		return Response.ok(entity).build();
		
	}
	
	@GET
	@Path("/EmployeeService/employeesByLocation/{empLocation}")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getEmployeesByEmpLocationXML(@PathParam(value = "empLocation") String empLocation) {
		
		Employee response = employeeService.findByEmpLocation(empLocation);
		GenericEntity<Employee> entity = new GenericEntity<Employee>(response){};
		return Response.ok(entity).build();
		
	}
	
	@GET
	@Path("/EmployeeService/employeesByBand/{empBand}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEmployeesByEmpBandJson(@PathParam(value = "empBand") String empBand) {
		
		Employee response = employeeService.findByEmpBand(empBand);
		GenericEntity<Employee> entity = new GenericEntity<Employee>(response){};
		return Response.ok(entity).build();
		
	}
	
	@GET
	@Path("/EmployeeService/employeesByBand/{empBand}")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getEmployeesByEmpBandXml(@PathParam(value = "empBand") String empBand) {
		
		Employee response = employeeService.findByEmpBand(empBand);
		GenericEntity<Employee> entity = new GenericEntity<Employee>(response){};
		return Response.ok(entity).build();
		
	}
	
	@POST
	@Path("/EmployeeService/employee")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response addEmployeesXml(@RequestBody Employee employee) {
		
		Boolean response = employeeService.save(employee);
		if (response) {
			return Response.ok("{\"Response\":\"Employee sucessfully saved\"}").build();
		} else {
			return Response.ok("{\"Response\":\"Employee NOT saved\"}").build();
		}
		
	}
	
	@PUT
	@Path("/EmployeeService/employee")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateEmployeesXml(@RequestBody Employee employee) {
		
		Boolean response = employeeService.update(employee);
		if (response) {
			return Response.ok("{\"Response\":\"Employee sucessfully updated\"}").build();
		} else {
			return Response.ok("{\"Response\":\"Employee NOT updated\"}").build();
		}
		
	}
	
	@DELETE
	@Path("/EmployeeService/employee/{empId}")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateEmployeesXml(@PathParam(value = "empId") String empId) {
			return Response.status(HttpStatus.SERVICE_UNAVAILABLE.value()).build();
	}
	
}