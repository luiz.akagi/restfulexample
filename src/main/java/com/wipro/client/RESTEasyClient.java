package com.wipro.client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import com.wipro.domain.Employee;

public class RESTEasyClient {

	private static Employee employee = new Employee("EM005", "Pam", "Poo", "London", "B1");
	private static String url = "http://localhost:25003/RESTfulExample/EmployeeService";

	public static void main(String[] args) {
		addEmployeesXML();
		updateEmployeesXML();
		
		getAllEmployeesXML();
		getAllEmployeesJson();
		
		getAllEmployeesByLocationXML();
		getAllEmployeesByLocationJson();
		
		getAllEmployeesByBandXML();
		getAllEmployeesByBandXML();
		
		deleteEmployees();

	}

	private static void getAllEmployeesXML() {
		try {

			ClientRequest request = new ClientRequest(url + "/employees");
			request.accept("application/xml");
			request.header("Content-Type", "application/xml");
			ClientResponse<String> response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getAllEmployeesJson() {
		try {

			ClientRequest request = new ClientRequest(url + "/employees");
			request.accept("application/json");
			request.header("Content-Type", "application/json");
			ClientResponse<String> response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getAllEmployeesByLocationXML() {
		try {

			ClientRequest request = new ClientRequest(url + "/employeesByLocation/London");
			request.accept("application/xml");
			request.header("Content-Type", "application/xml");
			ClientResponse<String> response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getAllEmployeesByLocationJson() {
		try {

			ClientRequest request = new ClientRequest(url + "/employeesByLocation/London");
			request.accept("application/json");
			request.header("Content-Type", "application/json");
			ClientResponse<String> response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getAllEmployeesByBandXML() {
		try {

			ClientRequest request = new ClientRequest(url + "/employeesByBand/B2");
			request.accept("application/xml");
			request.header("Content-Type", "application/xml");
			ClientResponse<String> response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getAllEmployeesByBandJSON() {
		try {

			ClientRequest request = new ClientRequest(url + "/employeesByBand/B2");
			request.accept("application/json");
			request.header("Content-Type", "application/json");
			ClientResponse<String> response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void addEmployeesXML() {
		try {

			ClientRequest request = new ClientRequest(url + "/employee");
			request.accept("application/xml");

			request.body("application/xml", employee);

			ClientResponse<String> response = request.post(String.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void updateEmployeesXML() {
		try {

			employee.setEmpBand("B2");
			
			ClientRequest request = new ClientRequest(url + "/employee");
			request.accept("application/xml");

			request.body("application/xml", employee);

			ClientResponse<String> response = request.put(String.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			BufferedReader br = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(response.getEntity(String.class).getBytes())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void deleteEmployees() {
		try {

			ClientRequest request = new ClientRequest(url + "/employee/EM001");
			request.accept("application/xml");
			request.header("Content-Type", "application/xml");
			ClientResponse<String> response = request.delete();
			if (response.getStatus() != 503) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			System.out.println("Output from Server should be 503 as DELETE command is not supported. \n");
				System.out.println(response.getStatus());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
