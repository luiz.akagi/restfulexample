package com.wipro.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestClientException;

import com.wipro.domain.Student;
import com.wipro.service.StudentService;

@Path("/StudentService")
public class StudentController {
	
	@Autowired
	StudentService studentService;
	
	@GET
	@Path("/StudentService/students")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> getApplicationJsonValue() throws RestClientException, Exception {
		
		ArrayList<Student> response = studentService.getAllStudents();
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

	@GET
	@Path("/StudentService/students/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> getStudentById(@PathVariable String id) throws RestClientException, Exception {
		
		Student response = studentService.getStudent(id);
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	
	@GET
	@Path("/StudentService/students/totalMark/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> getStudentByTotalMark(@PathVariable String id) throws RestClientException, Exception {
		
		List<Student> response = studentService.getStudentByTotalMark(id);
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	
	@GET
	@Path("/StudentService/students/Class/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> getStudentByClass(@PathVariable String id, @MatrixParam("studentClass") String studentClass) throws RestClientException, Exception {
		
		ArrayList<Student> response = studentService.getStudentByClass(id);
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	
	@POST
	@Path("/StudentService/student")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> getApplicationXmlValue( @RequestBody String payload) throws RestClientException, Exception {
//		Student student = mapper.readValue(payload, new TypeReference<Student>() {});
//		studentService.addStudent(student);
		
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DELETE
	@Path("/StudentService/student/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> deleteStudentById(@PathVariable String id) throws RestClientException, Exception {
		
		studentService.removeStudent(id);
		
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@PUT
	@Path("/StudentService/student/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Object> updateStudent( @RequestBody Student student) throws RestClientException, Exception {
		
		Student response = studentService.updateStudent(student);
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	
}
