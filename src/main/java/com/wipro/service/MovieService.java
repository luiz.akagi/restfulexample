package com.wipro.service;

import java.util.ArrayList;
import java.util.List;

import com.wipro.domain.Movie;

public class MovieService {

	List<Movie> movies = null;

	public List<Movie> getMovies() {
		if (movies == null) {
			movies = new ArrayList<Movie>();
			for (int i = 0; i < 10; i++) {
				movies.add(new Movie(i));
			}
		}
		return movies;
	}

	public Movie getMovie(String id) {
		return movies.get(Integer.parseInt(id));
	}

}
