package com.wipro.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "movie")
public class Movie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2418071580815680844L;

	private String movieId;
	private String movieName;
	private String movieActor;
	private Float movieCollection;

	public Movie() {
	}

	public Movie(int value) {
		super();
		this.movieId = String.valueOf(value);
		this.movieName = String.valueOf(value);
		this.movieActor = String.valueOf(value);
		this.movieCollection = (float) value;
	}

	public String getMovieId() {
		return movieId;
	}

	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getMovieActor() {
		return movieActor;
	}

	public void setMovieActor(String movieActor) {
		this.movieActor = movieActor;
	}

	public Float getMovieCollection() {
		return movieCollection;
	}

	public void setMovieCollection(Float movieCollection) {
		this.movieCollection = movieCollection;
	}

}
